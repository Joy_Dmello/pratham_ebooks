  window.FontAwesomeConfig = {
    searchPseudoElements: true
  }

  //Navigation Bar
 $(document).ready(function(){
	if($(window).width() > 1024){
		$(this).scroll(function() {
			var pos = $(this).scrollTop();
			$('.navbar.desktop-only').css({
				'position':'fixed',
				'height':'80px',
				'top':'34px',
				'margin-top':'0',
				'transition': 'all 1s ease',
				'background': '#FFF'
			})

			
			$('.logo').css({
				'padding-top':'0',
				'width':'140px',
				'margin':'0 auto',
				'transition': 'all 1s ease'
			})

			$('.navbar.desktop-only ul').css({
				'margin-top':'35px',
				'transition': 'all 1s ease'
			})

			$('.navbar.desktop-only form').css({
				'margin-top':'29px',
				'transition': 'all 1s ease'
			})

			$('#top-bar').css({
				'position':'fixed',
				'transition': 'all 2s ease',
				'background': '#E5E3E3',
				'z-index':'1',
				'top':'0'
			});


			if(pos == 0){
				$('.navbar.desktop-only').css({
				'position':'absolute',
				'height':'100px',
				'top':'34px',
				'background': '#FFF',
				'transition': 'all 1s ease',
			})


			$('#top-bar').css({
				'height':'34px',
				'position':'absolute',
				'background': '#E5E3E3',
				'transition': 'all 2s ease',
				'z-index':'1',
				'top':'0'
			});

			$('.logo').css({
				'padding-top':'0px',
				'width':'auto',
				'transition': 'all 1s ease'
			})

			$('.navbar.desktop-only ul').css({
				'margin-top':'42px',
				'transition': 'all 1s ease'
			})

			$('.navbar.desktop-only form').css({
				'margin-top':'36px',
				'transition': 'all 1s ease'
			})
			}
		});
	}

	// TOGGLING FAVOURITE ICON	
	$(".fav-floater").click(function () {
	   $(this).find('path').toggleClass("yellow");
	});

	$(".fav-floater").hover(function ()	{
	    $(this).find('path').addClass("cls-1");
	},
	function (){
		$(this).find('path').removeClass("cls-1");
	});

	$('.featured_slider').slick({
	  dots: false,
	  infinite: false,
	  speed: 300,
	  arrows: true,
	  prevArrow:"<button type='button' class='slick-prev float-left'> <span class='fa-stack fa-lg'><i class='fa fa-circle fa-stack-2x'></i><i class='fa fa-chevron-left fa-stack-1x fa-inverse'></i></span></button>",
	  nextArrow:"<button type='button' class='slick-next float-right'><span class='fa-stack fa-lg'><i class='fa fa-circle fa-stack-2x'></i><i class='fa fa-chevron-right fa-stack-1x fa-inverse'></i></span></i></button>",
	  slidesToShow: 4,
	  slidesToScroll: 4,
	  adaptiveHeight:true,
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3,
	        infinite: false,
	        dots: false,
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2,
	        infinite: false,
	        arrows: false,
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 4,
	        slidesToScroll: 1,
	        arrows: false,
	        infinite: false,
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});

	$('.promotional_slider').slick({
	  dots: true,
	  arrows:false,
	  infinite: true,
	  speed: 300,
	  slidesToShow: 1,
	  adaptiveHeight: true,
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow:1,
	        slidesToScroll: 1,
	        arrows: false,
	        infinite: false,
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow:1,
	        slidesToScroll: 1,
	        arrows: false,
	        infinite: false,
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow:1,
	        slidesToScroll: 1,
	        arrows: false,
	        infinite: false,
	      }
	  	}
	  ]
	});

	$('.review_slider').slick({
	  dots: false,
	  arrows:true,
	  infinite: true,
	  speed: 300,
	  slidesToShow: 1,
	  prevArrow:"<button type='button' class='slick-prev outline_btn br'>PREVIOUS</button>",
	  nextArrow:"<button type='button' class='slick-next outline_btn bl'>NEXT</button>",
	});

	$('.testimonial_slider').slick({
	  dots: false,
	  arrows:false,
	  infinite: false,
	  speed: 300,
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  adaptiveHeight:true,
	});

	
});

